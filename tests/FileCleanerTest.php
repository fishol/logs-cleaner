<?php

namespace Fishol\LogCleanup\Tests;

use DateTime;
use Fishol\LogCleanup\Cleaner\FileCleaner;
use Fishol\LogCleanup\Exception\InvalidLogFile;
use PHPUnit\Framework\TestCase;

class FileCleanerTest extends TestCase
{

    private const LOG_FILE_NAME = 'tests/testlog.log';
    private const LOG_CONTENT = <<<END
[2021-11-21T20:12:00.156352+00:00] security.DEBUG: Checking support on authenticator. {"firewall_name":"main","authenticator":"Symfony\\Component\\Security\\Http\\Authenticator\\FormLoginAuthenticator"} []
[2021-11-23T20:12:00.156388+00:00] security.DEBUG: Authenticator does not support the request. {"firewall_name":"main","authenticator":"Symfony\\Component\\Security\\Http\\Authenticator\\FormLoginAuthenticator"} []
[2021-11-25T22:12:00.163038+00:00] security.DEBUG: Read existing security token from the session. {"key":"_security_main","token_class":"Symfony\\Component\\Security\\Core\\Authentication\\Token\\UsernamePasswordToken"} []
[2021-11-25T22:12:00.194689+00:00] doctrine.DEBUG: SELECT t0.id AS id_1, t0.username AS username_2, t0.roles AS roles_3, t0.password AS password_4 FROM user t0 WHERE t0.id = ? [1] []
[2021-11-25T23:12:00.199208+00:00] security.DEBUG: User was reloaded from a user provider. {"provider":"Symfony\\Bridge\\Doctrine\\Security\\User\\EntityUserProvider","username":"admin"} []
[2021-11-25T23:13:29.325421+00:00] php.INFO: Deprecated: Unparenthesized `a ? b : c ? d : e` is deprecated. Use either `(a ? b : c) ? d : e` or `a ? b : (c ? d : e)` {"exception":"[object] (ErrorException(code: 0): Deprecated: Unparenthesized `a ? b : c ? d : e` is deprecated. Use either `(a ? b : c) ? d : e` or `a ? b : (c ? d : e)` at /usr/src/app/src/Controller/PlanningController.php:26)"} []
[2021-11-27T22:13:29.333484+00:00] request.INFO: Matched route "dashboard". {"route":"dashboard","route_parameters":{"_route":"dashboard","_controller":"App\\Controller\\DashboardController::index"},"request_uri":"http://localhost/","method":"GET"} []
[2021-11-27T22:13:29.338507+00:00] security.DEBUG: Checking for authenticator support. {"firewall_name":"main","authenticators":1} []
[2021-11-27T22:13:29.338567+00:00] security.DEBUG: Checking support on authenticator. {"firewall_name":"main","authenticator":"Symfony\\Component\\Security\\Http\\Authenticator\\FormLoginAuthenticator"} []
[2021-11-27T22:13:29.338597+00:00] security.DEBUG: Authenticator does not support the request. {"firewall_name":"main","authenticator":"Symfony\\Component\\Security\\Http\\Authenticator\\FormLoginAuthenticator"} []
[2021-11-27T22:13:29.343333+00:00] security.DEBUG: Read existing security token from the session. {"key":"_security_main","token_class":"Symfony\\Component\\Security\\Core\\Authentication\\Token\\UsernamePasswordToken"} []
[2021-11-28T22:13:29.372791+00:00] doctrine.DEBUG: SELECT t0.id AS id_1, t0.username AS username_2, t0.roles AS roles_3, t0.password AS password_4 FROM user t0 WHERE t0.id = ? [1] []
[2021-11-29T22:13:29.376717+00:00] security.DEBUG: User was reloaded from a user provider. {"provider":"Symfony\\Bridge\\Doctrine\\Security\\User\\EntityUserProvider","username":"admin"} []'
END;

    public function testThereIsFreshLogFile()
    {
        $this->assertStringEqualsFile(self::LOG_FILE_NAME, self::LOG_CONTENT);
    }

    public function testCleanerIsConfigured(): FileCleaner
    {
        $this->expectNotToPerformAssertions();
        $cleaner = new FileCleaner();
        $cleaner->setFiles([self::LOG_FILE_NAME]);

        return $cleaner;
    }

    public function testNonExistingLogFile()
    {
        $this->expectException(InvalidLogFile::class);
        $cleaner = new FileCleaner();
        $cleaner->setFiles(['invalid']);
    }

    public function testExistingLogFile()
    {
        $this->expectException(InvalidLogFile::class);
        $cleaner = new FileCleaner();
        $cleaner->setFiles(['invalid']);
    }

    /**
     * @depends testCleanerIsConfigured
     * @dataProvider dataProvider
     */
    public function testLogsCleanedNext(string $date, int $logsLeft, FileCleaner $cleaner)
    {
        $cleaner->cleanOlder(new DateTime($date));
        $this->assertEquals($logsLeft, $this->countLogs());
    }

    public function setUp(): void
    {
        file_put_contents(self::LOG_FILE_NAME, self::LOG_CONTENT);
    }

    public function tearDown(): void
    {
        unlink(self::LOG_FILE_NAME);
    }

    private function countLogs(): int
    {
        $counter = 0;
        $logFile = fopen(self::LOG_FILE_NAME, 'r');
        while (!feof($logFile)) {
            $line = fgets($logFile);
            if (!empty($line)) {
                $counter++;
            }
        }
        fclose($logFile);

        return $counter;
    }

    public function dataProvider(): array
    {
        return [
            ['1990-01-01', 13],
            ['2021-11-21', 13],
            ['2021-11-22', 12],
            ['2021-11-25', 11],
            ['2021-11-26', 7],
            ['2021-11-30', 0],
        ];
    }
}