<?php

namespace Fishol\LogCleanup\Cleaner;

interface CleanerInterface
{

    public function cleanOlder(\DateTime $to, bool $inclusive = false);

}