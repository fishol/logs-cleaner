<?php

namespace Fishol\LogCleanup\Cleaner;

use DateTime;
use Exception;
use Fishol\LogCleanup\Exception\InvalidLogFile;
use Fishol\LogCleanup\Exception\LogCleanupException;

class FileCleaner implements CleanerInterface
{

    private array $files;

    private string $dateFindRegex;

    public function __construct()
    {
        // Use default datetime format for symfony logs
        $this->dateFindRegex = '/\[(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{6}\+\d{2}:\d{2})\]/';
    }

    /**
     * @param string $regex
     */
    public function setDateFindRegex(string $regex): void
    {
        $this->dateFindRegex = $regex;
    }

    /**
     * @param string[] $files
     * @throws InvalidLogFile
     */
    public function setFiles(array $files): void
    {
        $this->files = [];
        foreach ($files as $file) {
            if (!is_string($file)) {
                throw new \InvalidArgumentException('Strings expected, ' . gettype($file) . ' given.');
            }
            if (is_readable($file)) {
                $this->files[] = $file;
            } else {
                throw new InvalidLogFile($file);
            }
        }
    }

    /**
     * @param DateTime $to
     * @param bool $inclusive
     * @throws LogCleanupException
     * @throws Exception
     */
    public function cleanOlder(DateTime $to, bool $inclusive = false)
    {
        foreach ($this->files as $file) {
            $newLogFile = fopen($file.'.tmp', 'w');
            $logFile = fopen($file, "r");
            if ($logFile) {
                while (($line = fgets($logFile)) !== false) {
                    $date = $this->findDate($line);
                    if ($date > $to || (!$inclusive && $date == $to)) {
                        fwrite($newLogFile, $line);
                    }
                }
                fclose($newLogFile);
                fclose($logFile);
                unlink($file);
                rename($file.'.tmp', $file);
            } else {
                throw new LogCleanupException('Problem during file processing');
            }
        }
    }

    /**
     * @param $line
     * @return DateTime|null
     * @throws Exception
     */
    private function findDate($line): ?DateTime
    {
        preg_match($this->dateFindRegex, $line, $matches);
        return new DateTime($matches[1]);
    }
}