<?php

namespace Fishol\LogCleanup;

use Fishol\LogCleanup\Cleaner\FileCleaner;

class LogCleanup
{

    public static function cleanup()
    {
        $cleaner = new FileCleaner();

        $cleaner->setFiles(['tests/test.log']);

        $cleaner->cleanOlder(new \DateTime('2021-11-27'));
    }

}